const wordsArr = [
  "jeden",
  "dwa",
  "trzy",
  "cztery",
  "rzodkiewka",
  "seler",
  "kapusta",
  "ogórek",
  "sałata",
  "galareta",
  "google",
  "amazon",
  "nodejs",
  "typescript",
  "javascript",
  "gulp",
  "react",
  "angular",
  "photoshop",
  "tygrys",
  "owca",
  "panda",
  "orka",
  "sokół",
  "puchacz",
  "kałamarnica"
];

let apiWordList = [];
let apiWordGet = () => {
  let pushNamesIntoArray = (item) => {
    apiWordList.push(item.name);
  }
  var request = new XMLHttpRequest();
  request.open('GET', 'http://navian.keimo.pl/wp-json/wp/v2/tags/?per_page=50', false);
  request.onload = function () {
    var dataDictonary = JSON.parse(this.response);
    dataDictonary.forEach(pushNamesIntoArray);
  }
  request.send();
}
apiWordGet();

let hiddenLetters = [];
let badLetter = [];
let maxLifes = 10
let lifes = 0;
let wins = 0;
// Create _ for the chosen word

let hiddenLettersGen = () => {
  for (let i = 0; i < randomWord.length; i++) {
    hiddenLetters.push("_");
  }
  blanks[0].innerHTML = hiddenLetters.join(" ");
  return hiddenLetters;
};

//Random word from array
let randomWordGen = () => {
  let randomNumber = Math.floor(Math.random() * apiWordList.length);
  randomWord = apiWordList[randomNumber];
  return randomWord;
}


// DOM changing
let blanks = document.getElementsByClassName('blanks');
let incorrect = document.getElementsByClassName('bad-letters');

// Choosing random word for the game



// Reset the game
let resetGame = () => {
  lifes = maxLifes;

  // Resetting arrays
  hiddenLetters = [];
  badLetter = [];
  incorrect[0].innerHTML = badLetter.join(", ");

  // Clear hangman img
  document.getElementById('hangman-img').src = '';

  // Choose new word
  randomWordGen();

  // Build _ for new word
  hiddenLettersGen().join(' ');
  //CHEAT CHEAT - Check the right word
  console.log(randomWord);
}

// Img updater
let updateHangmanImage =() => {
  document.getElementById('hangman-img').src = 'assets/images/' + (maxLifes - lifes) + ".png";
};

document.addEventListener('keypress', (event) => {
  let choosenLetter = String.fromCharCode(event.keyCode);

// Checking for good letter
  for (let i = 0; i < randomWord.length; i++) {
    const element = randomWord[i];
// If good letter
    if (element === choosenLetter) {
      hiddenLetters[i] = choosenLetter;
      blanks[0].innerHTML = hiddenLetters.join(' ');
    }
  }
  
// If all letters matches the word
  if (hiddenLetters.join('') == randomWord) {
    wins++;
    document.getElementById('win-counter').innerText = wins;
    let wonGameMessage = confirm('You win!!! Wanna play again?');
    if (wonGameMessage) {
      resetGame();
      return true;
    } else {
      alert('Too bad... =(');
      return false;
    }
  }

// Wrong letter
  if (randomWord.indexOf(choosenLetter) === -1) {
    badLetter.push(choosenLetter);
    incorrect[0].innerHTML = badLetter.join(', ');
    lifes--;
    updateHangmanImage();
// Lost game
    if (lifes === 0) {
      let lostGameMessage = confirm("I'm sorry, You lost. Wanna play again?");
      if (lostGameMessage) {
        resetGame();
        return true;
      } else {
        alert('As you wish.');
        return false;
      }
    }
  }
});